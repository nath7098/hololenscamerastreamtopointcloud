# HololensRGBCameraStream's results

|File Name 	| Total points	| Observations|
|------------|---------------|-------------|
|test%2-1.obj	| 32716		| inverted on X & Y + half image|
|test%2-2-23min.obj	| 100800	| inverted on X & Y + complete image & some color bugs|
|test%2-3.obj	| 68815		| inverted X & Y + missing some of the image bottom|
|test%10-3.obj   | 20160		| inverted X & Y + maybe missing some of the image bottom |




# Global comment :

Every obj file has been produced by taking only one picture.


The double for loop saving the points is going through every point 
of the frame (450 x 448 = 201600) so there is a modulo on the column 
loop which is set to %2 by default (450 x 224 = 100800) 

A swatch has been set in the code on the for loop that saves the points
and for a modulo 2 (100800 points) the time is 23 minutes average of saving