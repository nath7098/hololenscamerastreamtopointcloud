# HololensCameraStreamToPointCloud
This document will allow you to find the following things:
*  Unity packages
*  Documentation
*  Project description
*  How to setup the project
*  How to continue the project

## Project description
This project follows the steps of previous ones on the following problem :
Creating a points set representing an object the user is looking at through Hololens' RGBD Flow

previous work :
Grégoire Marco, Polytech Tours, Research & Development Project
Nathan Miguens, Mines de Nancy, Internship at the LIFAT : [wiki](https://github.com/NthnMgns/HoloLens_3D_Reconstitution/wiki)

## Environment
### Unity :
*  version : 2018.4.2f1 Personal

Note : If you want to use unity 2019.x then .NET scripting backend has been disabled so you won't be able to debug your csharp code

### Visual Studio :
*  Community 2017
*  version : 15.9.17
*  Microsoft .NET Framework - Version : 4.8.03752
*  Installed products :
    *   .NET Desktop development
    *   Desktop C++ development
    *   Windows Universal Platform development
    *   Game development with Unity

## Content
This repository contains the Unity Package created from the Unity project

The Unity package contains features to fit the various needs of your project. This can include any core features of Unity which is installed along with the Editor, or other packages which you can install as needed.
This package allows to vehicle the most important parts of the project and it is sufficient to continue the project as it is with some configurations to make.

### [HololensDepthCameraFlow.unitypackage](HololensDepthCameraFlow.unitypackage)

This package contains the code and assets for depth camera stream.

#### Input
The scene the user wants to scan

#### Output
A .obj file containing all the 3D points (x, y, z) colored in red (for the first capture) and green (for 2nd capture). The color ofthe point cloud is hardcoded because this app doesn't access the RGB camera Stream.

### [HololensRGBCameraFlow.unitypackage](HololensRGBCameraFlow.unitypackage)

#### Input
The scene the user wants to scan

#### Output
A .obj file containing all the 2D points (x, y) of the captures the user made. The points colors are defined by the RGB camera stream data.

# Getting started
In this section we will see how to get the project on local and how to setup the enviroments.

## Getting the project
Simply download the file ***HololensDepthCameraFlow.unitypackage***.

## Unity 

Then in Unity, create a **New Project** named as you wish, select the location, set Template to **3D** and create the project.

Once the project has launched, Under **Assests > Import Package > Custom Package** select the .unitypackage file.

In the popup, Select all checkboxes and click import.

In the project panel at the middle bottom of Unity, double-click **Reasearmode_demo**.

**Edit > Project Settings > Graphics > Built-in Shader Settings**
*   Size : **10** (then click anywhere to update the view)
*   Element 9 (click on the circle on right) : Search for ***Unlit/Texture*** and double-click it


**File > Build Settings** :
*   add open scenes
*   Platform : Universal Windows Plateform -> switch platform
*   Target device : Hololens
*   Architecture : x86
*   Build Type : D3D
*   Target SDK Version : Latest installed
*   Minimum Platform Version : *lowest value*
*   Visual Studio Version : Latest installed
*   Build and Run : Local Machine
*   Build configuration : *See* ***Unity : Release configurations*** *or* ***Unity : Debug configurations***
*   Player Settings...
    *   Other Settings :  *See* ***Unity : Release configurations*** *or* ***Unity : Debug configurations***
    *   Publishing settings :
        *   Capabilities :
            * [x] WebCam
            * [x] SpatialPerception   
    *   XR Settings :
        * [x] Virtual Reality Supported
        * Under Virtual Reality SDKs, Windows Mixed Reality should appear
*   In Build Setting window, click Build
*   Create a new directory called App if it doesn't aldready exists
*   Select the App directory
*   When it's over the project directory should open
*   Go in App directory and double-click the ***projectName.sln***

### Unity : Release configurations

**File > Build Settings** :
*   Build configuration : release
*   Player Settings...
    *   Other Settings :
        *   Scripting Backend : IL2CPP
        *   API Compatibility Level : .NET 4.x
        * [x] Allow unsafe code

### Unity : Debug configurations

**File > Build Settings** :
*   Build configuration : debug
*   Player Settings...
    *   Other Settings :
        *   Scripting Backend : .NET
        *   API Compatibility Level : .NET 4.x
        * [x] Allow unsafe code
*	[x] Copy Reference
*   [x] Unity C# Project
*   [x] Developement Build

## Visual Studio
The project should have opened up and the first thing to do is to check that in Solution explorer (right panel), the ***ProjectName (Universal Windows)*** is selected.
If not, **right click > set as StratUp Project**.

Then, expand it and on Package.appxmanifest right click > Open With... > XML text editor
*   In `<Package ...>` (line 2) add `xmlns:rescap="http://schemas.microsoft.com/appx/manifest/foundation/windows10/restrictedcapabilities"` and also line 2 add `rescap` in ` IgnorableNamespaces="uap uap2 uap3 uap4 mp mobile iot rescap"` 
*   Between `<Capabilities> </Capabilities>` add `<rescap:Capability Name="perceptionSensorsExperimental" />`
*   Save file (Crtl+S) and close it

### Visual Studio : Release configurations
For Release deployment on Hololens, you will need to plug them in and have this configuration
![test](/readmeImages/vsReleaseBar.JPG)

You can then click the green arrow to build and deploy the app to the Hololens.

### Visual Studio : Debug configurations
You will first need to Allow Unsafe code everywhere :
In Solution Explorer panel, On each Solution (line) :
*   Alt+Enter or Right click > Build :
    * [x] Allow Unsafe Code
    * Crtl + S

**This we have to be done every time your build from Unity**

The C# code is in assembly C-Sharp! You can then open your .cs file and put break points wherever you need to.  
When you modify your code you only have to save and relaunch your app but I suggest you to sometimes build from Unity when you've done big modifications.

For Debug deployment on Hololens, you will need to plug them in and have this configuration
![test](/readmeImages/vsDebugBar.JPG)

# Documentations

## HololensDepthCameraFlow code documentation

[hololensdocumentation.nathancouton.fr](http://hololensdocumentation.nathancouton.fr)

# Results visualisation

The results of the apps are .obj files that can be opened with multiple softwares (Meshlab, blender...). 
I used Meshlab 2016.12 to open the files as it is easier to use.
