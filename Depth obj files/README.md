# HololensDepthCameraStream's results

|File Name 	| Total points	| Observations|
|-----------|---------------|-------------|
|appV2-original.obj	| 26027		| No modification made to Nathan Miguens' version <br/> inverted on X & Y + offset between 2 captures
|noMove.obj	| 27136		| No movement between the 2 captures <br/> points clouds are similar which means that there is no <br/>  error in the matrices when not moving |
|bugOnSecond.obj	| 20425		| I saved this capture to show how 90% of my tests ended up <br/> on the second capture's cloud (green) there's a part of the cloud missing |
original-90deg.obj	| 27647		| Moved around the object by 90° <br/> the offset seems to be mirrored
|UnityToComera.obj  	| 20013		| Using the unity coordinate system to camera's instead of camera to unity <br/> the 2 clouds are packed but they are not complematry as expected |


# Global comment :

Every obj file contains the merge between 2 captures of the same object
but from different view for some 

The double for loop saving the points is going through every point 
of the frame (450 x 448 = 201600) so there is a modulo on the column 
loop which is set to %2 by default (450 x 224 = 100800) 

Moreover, neither the points with a depth > 2 meters nor the duplicates are saved in the file 
which explains the number of points around 20000 instead of 100800
